let counter = 0;


document.getElementById('addButton').addEventListener("click", () => {
  const product = document.getElementById("textInput").value;
  document.getElementById("textInput").value = '';

  if(product) {
  	counter++;
  	document.getElementById("counter").innerHTML = counter;

  	if(counter === 1) {
  		document.getElementById("contentAddOns").hidden = false;
  	}

  	//Add list items
  	const newEntry = document.createElement('li');
  	newEntry.appendChild(document.createTextNode(product));
  	newEntry.setAttribute("id", product);
  	document.getElementById("list").appendChild(newEntry);
  	

  	//Delete individual list items
  	newEntry.addEventListener("click", () => {
  		newEntry.parentNode.removeChild(newEntry);
  		counter--;
  		if(counter === 0) {
  			document.getElementById("contentAddOns").hidden = true;
  		}
  		document.getElementById("counter").innerHTML = counter;
  	})


  	//Animation trigger
  	setTimeout(() => {
    newEntry.className = newEntry.className + " show";
  	}, 10);

  }
});


//Remove all items
document.getElementById("removeAll").addEventListener("click", () => {
	if (window.confirm("Are you sure that you want to remove all products?")){
		document.getElementById("list").innerHTML = '';
		counter = 0;
		document.getElementById("contentAddOns").hidden = true;
	}
});

